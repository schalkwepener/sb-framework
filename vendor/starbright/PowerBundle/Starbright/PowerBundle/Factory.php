<?php

namespace Starbright\PowerBundle;

use Symfony\Component\HttpFoundation\StreamedResponse;

/**
 * Factory for PHPExcel objects, StreamedResponse, and PHPExcel_Writer_IWriter.
 *
 * @package Starbright\PowerBundle
 */
class Factory
{
    private $phpPowerIO;

    public function __construct($phpPowerIO = '\PHPPowerPoint_IOFactory')
    {
        $this->phpPowerIO = $phpPowerIO;
    }
    /**
     * Creates an empty PHPExcel Object if the filename is empty, otherwise loads the file into the object.
     *
     * @param string $filename
     *
     * @return \PHPExcel
     */
    public function createPHPPowerPointObject($filename =  null)
    {
        if (null == $filename) {
            $phpPowerObject = new \PHPPowerPoint();// \PHPPower();

            return $phpPowerObject;
        }

        return call_user_func(array($this->phpPowerIO, 'load'), $filename);
    }

    /**
     * Create a writer given the PHPExcelObject and the type,
     *   the type coul be one of PHPExcel_IOFactory::$_autoResolveClasses
     *
     * @param \PHPExcel $phpExcelObject
     * @param string    $type
     *
     *
     * @return \PHPExcel_Writer_IWriter
     */
    public function createWriter(\PHPPowerPoint $phpPowerObject, $type = 'PowerPoint2007')
    {
        return call_user_func(array($this->phpPowerIO, 'createWriter'), $phpPowerObject, $type);
    }

    /**
     * Stream the file as Response.
     *
     * @param \PHPExcel_Writer_IWriter $writer
     * @param int                      $status
     * @param array                    $headers
     *
     * @return StreamedResponse
     */
    public function createStreamedResponse(\PHPPowerPoint_Writer_IWriter $writer, $status = 200, $headers = array())
    {
        return new StreamedResponse(
            function () use ($writer) {
                $writer->save('php://output');
            },
            $status,
            $headers
        );
    }
}
