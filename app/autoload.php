<?php

use Doctrine\Common\Annotations\AnnotationRegistry;
use Composer\Autoload\ClassLoader;

/**
 * @var ClassLoader $loader
 */
$loader = require __DIR__.'/../vendor/autoload.php';

AnnotationRegistry::registerLoader(array($loader, 'loadClass'));
/*
$loader->
$loader->registerPrefixes('PHPPowerPoint_', __DIR__.'/vendor/phpoffice/phppowerpoint/Classes/PHPPowerPoint');
*/
/*
$loader->registerPrefixes(array(
    'Swift_' => __DIR__.'/vendor/swiftmailer/lib/classes',
    'Twig_'  => __DIR__.'/vendor/twig/lib',
));
*/

$w = __DIR__;
$w = str_replace("app", "", $w);
$w .= 'vendor/starbright/PowerBundle';
//var_dump($w);
$loader->add('Starbright\\PowerBundle',$w);


$loader->register();

return $loader;
