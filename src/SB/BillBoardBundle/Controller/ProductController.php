<?php

namespace SB\BillBoardBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use SB\BillBoardBundle\Entity\Product;
use SB\BillBoardBundle\Form\ProductType;
use SB\BillBoardBundle\Form\ProvinceType;
use SB\GoogleMapGeneratorBundle;
use SB\UploaderBundle\Form\Type\UploaderType;

use SB\PriceUpdaterBundle\Entity\PriceReport;
/**
 * Product controller.
 *
 * @Route("/product")
 */
class ProductController extends Controller
{

    /**
     * Lists all Product entities.
     *
     * @Route("/", name="product")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('SBBillBoardBundle:Product')->findAll();
       // $entities = $em->getRepository('SBBillBoardBundle:Product')->find(1);
        return array(
            'entities' => $entities,
        );
    }

    public function generate_image(Product $entity){
        //ok saved the product, now craete the map and update the product

        $map_container = new \SB\GoogleMapGeneratorBundle\SBGoogleMapGeneratorBundle();
        $map_location =  $map_container->mapyGetAction($entity->getId(),$entity->getGpsLat(),$entity->getGpsLong(),'mapy');

        return $map_location;
    }

    /**
     * Creates a new Product entity.
     *
     * @Route("/", name="product_create")
     * @Method("POST")
     * @Template("SBBillBoardBundle:Product:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Product();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();


            var_dump($_REQUEST);
            die();
            $map_location = $this->generate_image($entity);

            $entity->setMapLocation($map_location['data']['location']);
            $em->persist($entity);
            $em->flush();
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
    * Creates a form to create a Product entity.
    *
    * @param Product $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Product $entity)
    {
        $form = $this->createForm(new ProductType(), $entity, array(
            'action' => $this->generateUrl('product_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Product entity.
     *
     * @Route("/new", name="product_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Product();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Product entity.
     *
     * @Route("/{id}", name="product_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SBBillBoardBundle:Product')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Product entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Product entity.
     *
     * @Route("/{id}/edit", name="product_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SBBillBoardBundle:Product')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Product entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Product entity.
    *
    * @param Product $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Product $entity)
    {
        $form = $this->createForm(new ProductType(), $entity, array(
            'action' => $this->generateUrl('product_update', array('id' => $entity->getId())),
            'method' => 'PUT',
            'attr' => array(
                    'id' => 'view_product',
                    'data-list-url' => $this->generateUrl('product_image_list', array('id' => $entity->getId()))
                ),
        ));
     //   $form->add('uploader', new UploaderType() , array('label' => 'koos doos'));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
	/**
     * retrieves list of product images.
     *
     * @Route("/{id}/images", name="product_image_list")
     * @Method("GET")
     */
     public function uploadedImagesAction($id){
      //  $statement = $connection->prepare("SELECT * FROM ".$table);
      //  $statement->execute();
     //   $list = $statement->fetchAll();

        $list = 'none';
        $return_array = array("list"=>$list);

        return $this->render('SBMultiUploaderBundle:Form:imageList.html.twig', array(
            'entities' => $return_array
        ));

        //return $return_array;
    }
    /**
     * Edits an existing Product entity.
     *
     * @Route("/{id}", name="product_update")
     * @Method("PUT")
     * @Template("SBBillBoardBundle:Product:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SBBillBoardBundle:Product')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Product entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            $map_location = $this->generate_image($entity);

            $entity->setMapLocation($map_location['data']['location']);
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('product_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Product entity.
     *
     * @Route("/{id}", name="product_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('SBBillBoardBundle:Product')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Product entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('product'));
    }

    /**
     * Creates a form to delete a Product entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('product_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }

    public function searchDisplay(){
        //return $this->render('SBBillBoardBundle:Product:index.html.twig', array());
        //return "SSSSS";
    }

    /**
     * Edits an existing Product entity.
     *
     * @Route("/fancy/{data}", name="fancy")
     * @Method("GET")
     *
     */
     public function fancyAction($data){
        return $this->render('SBBillBoardBundle:Form:custom_dropdown.html.twig', array(
            'entities' => $data
        ));
     }


	public function get_list($connection,$table){
        $statement = $connection->prepare("SELECT * FROM ".$table);
        $statement->execute();
        $list = $statement->fetchAll();
        $return_array = array("xxx" => $table,"list"=>$list);
        return $return_array;
	}


    public function searchProductAction(){

        $em = $this->getDoctrine()->getManager();
        $connection = $em->getConnection();

        $provinces = $this->get_list($connection,'Province');
        $category = $this->get_list($connection,'Category');
        $type = $this->get_list($connection,'Type');


        return $this->render('SBBillBoardBundle:Form:search.html.twig', array(
             'data_set' =>
                array(
                    'province' => $provinces,
                    'cateogry' => $category,
                    'type' => $type
               )
            )
        );
    }

	public function returnSearchSQL($request){

        $all = $request->request->all();

        $province = $all['province'];
        $category  = $all['category'];
        $type = $all['type'];

        $sql_search = "";

        $sql_search .= " SELECT
            product.id,
            product.name,
            product.number,
            product.site_number as siteNumber,
            province.province as provinceId,
            product.city,
            product.suburb,
            product.street,
            product.cnr_street as cnrStreet,
            product.gps_long as gpsLong,
            product.gps_lat as gpsLat,
            illumination.illumination_type as illuminationId,
            type.type as typeId,
            size.size as sizeId,
            category.category as categoryId,
            ratetype.ratetype as rateTypeId,
            product.price
        FROM product
        INNER JOIN province ON province.id = product.province_id
        INNER JOIN illumination ON illumination.id = product.illumination_id
        INNER JOIN TYPE ON type.id = product.type_id
        INNER JOIN size ON size.id = product.size_id
        INNER JOIN category ON category.id = product.cateogry_id
        INNER JOIN ratetype ON ratetype.id = product.rate_type_id
        WHERE
        ";

        $cont = false;
        $parameters = "";

        if($province != ''){
             $parameters .= " product.province_id = '".$province."'";
             $cont = true;
        }else{
            $cont = false;
        }

        if($category != ''){
            if($cont){
                $parameters .= " and ";
            }
            $parameters .= " product.cateogry_id = '".$category."'";
            $cont = true;
        }else{
            $cont = false;
        }

        if($type != ''){
            if($cont){
                $parameters .= " and ";
            }
            $parameters .= " product.type_id = '".$type."'";
            $cont = true;
        }else{
             $cont = false;
        }

        $sql_search .= $parameters;

        if($parameters == '' ){
             $sql_search .= "  1 = 1 ";
        }
        return $sql_search;
    }

     /**
     * Search this shit
     *
     * @Route("/search/ajax", name="search")
     * @Method("POST")
     */
    public function ajaxSearchAction(Request $request){

        $em = $this->getDoctrine()->getManager();
        $connection = $em->getConnection();
//
//        $all = $request->request->all();
//
//        $province = $all['province'];
//        $category  = $all['category'];
//        $type = $all['type'];
//
//        $sql_search = "";
//
//
//        $sql_search .= " SELECT product.id, product.name, product.number, product.site_number, province.province, product.city, product.suburb, product.street, product.cnr_street, product.gps_long, product.gps_lat, illumination.illumination_type, type.type, size.size, category.category, ratetype.ratetype
//        FROM product
//        INNER JOIN province ON province.id = product.province_id
//        INNER JOIN illumination ON illumination.id = product.illumination_id
//        INNER JOIN TYPE ON type.id = product.type_id
//        INNER JOIN size ON size.id = product.size_id
//        INNER JOIN category ON category.id = product.cateogry_id
//        INNER JOIN ratetype ON ratetype.id = product.rate_type_id
//        WHERE
//        ";
//
//        $cont = false;
//        $parameters = "";
//
//        if($province != ''){
//             $parameters .= " product.province_id = '".$province."'";
//             $cont = true;
//        }else{
//            $cont = false;
//        }
//
//        if($category != ''){
//            if($cont){
//                $parameters .= " and ";
//            }
//            $parameters .= " product.cateogry_id = '".$category."'";
//            $cont = true;
//        }else{
//            $cont = false;
//        }
//
//        if($type != ''){
//            if($cont){
//                $parameters .= " and ";
//            }
//            $parameters .= " product.type_id = '".$type."'";
//            $cont = true;
//        }else{
//             $cont = false;
//        }
//
//        $sql_search .= $parameters;
//
//        if($parameters == '' ){
//             $sql_search .= "  1 = 1 ";
//        }
		$statement = $connection->prepare($sql_search);
        $statement->execute();
        $list = $statement->fetchAll();
        $searched_list = $list;

        return $this->render('SBBillBoardBundle:Product:product_rows.html.twig', array(
            'entities' => $searched_list
        ));
    }

	/**
     * Edits an existing Product entity price.
     *
     * @Route("/price/controls/", name="price_controls")
     * @Method("GET")
     *
     */
     public function priceUdaterAction($data){
        return $this->render('SBBillBoardBundle:Form:price_updater.html.twig', array(
          //  'entities' => $data
        ));
	}
    public function priceControlsAction(){
        return $this->render('SBBillBoardBundle:Form:price_updater.html.twig', array(
        ));
    }

     /**
     * Search this shit
     *
     * @Route("/price/update/ajax", name="price_update")
     * @Method("POST")
     */
    public function ajaxPriceUpdateAction(Request $request){

        $em = $this->getDoctrine()->getManager();
        $connection = $em->getConnection();

        //first get all the product id's
        $all = $_REQUEST['products'];
        $all_array = explode("_",$all);
        $all_array_clean = array_filter($all_array);

        //are we going up or down
        $direction = $_REQUEST['direction'];

        //by how much
        $modify_by_val = $_REQUEST['modify_by_val'];

        //with what method
        $modifier = $_REQUEST['modifier'];

        //ok now run through all the prodcuts selected 1 by 1 and do the price update thingy.
        foreach($all_array_clean as $id){
            $entity = $em->getRepository('SBBillBoardBundle:Product')->find($id);
            $current_price  =  $entity->getPrice();
            $new_price = 0;

            if($modifier == 'fixed'){ //this is fixed modification
                if($direction == 'up'){ //add this to the price
                    $new_price = $current_price + $modify_by_val;
                }else{// deduct this from the price
                    $new_price = $current_price - $modify_by_val;
                }
            }else{//this is a percentage movement
                $percentage_movement = (($current_price * $modify_by_val)/ 100);
                if($direction == 'up'){ //add this percentage to the price
                    $new_price = $current_price + $percentage_movement;
                 }else{// deduct this from the price
                    $new_price = $current_price - $percentage_movement;
                }
            }

            $entity->setPrice($new_price);

            $em->persist($entity);
            $em->flush();


            $price_report = $this->get("price_report")->createPriceReportAction($id,$direction,$modifier,$modify_by_val,666);
        }

        $sql_search = $this->returnSearchSQL($request);

        $statement = $connection->prepare($sql_search);
        $statement->execute();
        $list = $statement->fetchAll();
        $searched_list = $list;

        return $this->render('SBBillBoardBundle:Product:product_rows.html.twig', array(
            'entities' => $searched_list
        ));
    }
}
