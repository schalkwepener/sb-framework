<?php

namespace SB\BillBoardBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use SB\BillBoardBundle\Entity\DisplayTemplate;
use SB\BillBoardBundle\Form\DisplayTemplateType;

/**
 * DisplayTemplate controller.
 *
 * @Route("/displaytemplate")
 */
class DisplayTemplateController extends Controller
{

    /**
     * Lists all DisplayTemplate entities.
     *
     * @Route("/", name="displaytemplate")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('SBBillBoardBundle:DisplayTemplate')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new DisplayTemplate entity.
     *
     * @Route("/", name="displaytemplate_create")
     * @Method("POST")
     * @Template("SBBillBoardBundle:DisplayTemplate:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new DisplayTemplate();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('displaytemplate_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
    * Creates a form to create a DisplayTemplate entity.
    *
    * @param DisplayTemplate $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(DisplayTemplate $entity)
    {
        $form = $this->createForm(new DisplayTemplateType(), $entity, array(
            'action' => $this->generateUrl('displaytemplate_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new DisplayTemplate entity.
     *
     * @Route("/new", name="displaytemplate_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new DisplayTemplate();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a DisplayTemplate entity.
     *
     * @Route("/{id}", name="displaytemplate_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SBBillBoardBundle:DisplayTemplate')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find DisplayTemplate entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing DisplayTemplate entity.
     *
     * @Route("/{id}/edit", name="displaytemplate_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SBBillBoardBundle:DisplayTemplate')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find DisplayTemplate entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a DisplayTemplate entity.
    *
    * @param DisplayTemplate $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(DisplayTemplate $entity)
    {
        $form = $this->createForm(new DisplayTemplateType(), $entity, array(
            'action' => $this->generateUrl('displaytemplate_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing DisplayTemplate entity.
     *
     * @Route("/{id}", name="displaytemplate_update")
     * @Method("PUT")
     * @Template("SBBillBoardBundle:DisplayTemplate:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SBBillBoardBundle:DisplayTemplate')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find DisplayTemplate entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('displaytemplate_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a DisplayTemplate entity.
     *
     * @Route("/{id}", name="displaytemplate_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('SBBillBoardBundle:DisplayTemplate')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find DisplayTemplate entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('displaytemplate'));
    }

    /**
     * Creates a form to delete a DisplayTemplate entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('displaytemplate_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
