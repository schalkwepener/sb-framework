<?php

namespace SB\BillBoardBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use SB\BillBoardBundle\Entity\Illumination;
use SB\BillBoardBundle\Form\IlluminationType;

/**
 * Illumination controller.
 *
 * @Route("/illumination")
 */
class IlluminationController extends Controller
{

    /**
     * Lists all Illumination entities.
     *
     * @Route("/", name="illumination")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('SBBillBoardBundle:Illumination')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new Illumination entity.
     *
     * @Route("/", name="illumination_create")
     * @Method("POST")
     * @Template("SBBillBoardBundle:Illumination:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Illumination();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('illumination_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
    * Creates a form to create a Illumination entity.
    *
    * @param Illumination $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Illumination $entity)
    {
        $form = $this->createForm(new IlluminationType(), $entity, array(
            'action' => $this->generateUrl('illumination_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Illumination entity.
     *
     * @Route("/new", name="illumination_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Illumination();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Illumination entity.
     *
     * @Route("/{id}", name="illumination_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SBBillBoardBundle:Illumination')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Illumination entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Illumination entity.
     *
     * @Route("/{id}/edit", name="illumination_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SBBillBoardBundle:Illumination')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Illumination entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Illumination entity.
    *
    * @param Illumination $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Illumination $entity)
    {
        $form = $this->createForm(new IlluminationType(), $entity, array(
            'action' => $this->generateUrl('illumination_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Illumination entity.
     *
     * @Route("/{id}", name="illumination_update")
     * @Method("PUT")
     * @Template("SBBillBoardBundle:Illumination:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SBBillBoardBundle:Illumination')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Illumination entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('illumination_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Illumination entity.
     *
     * @Route("/{id}", name="illumination_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('SBBillBoardBundle:Illumination')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Illumination entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('illumination'));
    }

    /**
     * Creates a form to delete a Illumination entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('illumination_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
