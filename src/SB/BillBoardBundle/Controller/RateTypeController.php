<?php

namespace SB\BillBoardBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use SB\BillBoardBundle\Entity\RateType;
use SB\BillBoardBundle\Form\RateTypeType;

/**
 * RateType controller.
 *
 * @Route("/ratetype")
 */
class RateTypeController extends Controller
{

    /**
     * Lists all RateType entities.
     *
     * @Route("/", name="ratetype")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('SBBillBoardBundle:RateType')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new RateType entity.
     *
     * @Route("/", name="ratetype_create")
     * @Method("POST")
     * @Template("SBBillBoardBundle:RateType:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new RateType();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('ratetype_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
    * Creates a form to create a RateType entity.
    *
    * @param RateType $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(RateType $entity)
    {
        $form = $this->createForm(new RateTypeType(), $entity, array(
            'action' => $this->generateUrl('ratetype_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new RateType entity.
     *
     * @Route("/new", name="ratetype_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new RateType();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a RateType entity.
     *
     * @Route("/{id}", name="ratetype_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SBBillBoardBundle:RateType')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find RateType entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing RateType entity.
     *
     * @Route("/{id}/edit", name="ratetype_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SBBillBoardBundle:RateType')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find RateType entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a RateType entity.
    *
    * @param RateType $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(RateType $entity)
    {
        $form = $this->createForm(new RateTypeType(), $entity, array(
            'action' => $this->generateUrl('ratetype_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing RateType entity.
     *
     * @Route("/{id}", name="ratetype_update")
     * @Method("PUT")
     * @Template("SBBillBoardBundle:RateType:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SBBillBoardBundle:RateType')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find RateType entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('ratetype_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a RateType entity.
     *
     * @Route("/{id}", name="ratetype_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('SBBillBoardBundle:RateType')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find RateType entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('ratetype'));
    }

    /**
     * Creates a form to delete a RateType entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('ratetype_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
