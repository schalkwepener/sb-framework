<?php

namespace SB\BillBoardBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * Product
 *
 * @ORM\Table(name="product")
 * @ORM\Entity
 *
 * @Gedmo\Loggable()
 * @Gedmo\SoftDeleteable(fieldName="deletedAt")
 */

class Product
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Gedmo\Versioned
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     * @Gedmo\Versioned
     * @ORM\Column(name="number", type="string", length=255)
     */
    private $number;

    /**
     * @var string
     * @Gedmo\Versioned
     * @ORM\Column(name="site_number", type="string", length=255)
     */
    private $siteNumber;

     /**
     * @var integer
      * @Gedmo\Versioned
     * @ORM\ManyToOne(targetEntity="SB\BillBoardBundle\Entity\Province", inversedBy="product")
     * @ORM\JoinColumn(name="province_id", referencedColumnName="id")
     *
     **/

    private $provinceId;

    /**
     * @var string
     * @Gedmo\Versioned
     * @ORM\Column(name="city", type="string", length=255)
     */
    private $city;

    /**
     * @var string
     *@Gedmo\Versioned
     * @ORM\Column(name="suburb", type="string", length=255)
     */
    private $suburb;

    /**
     * @var string
     *@Gedmo\Versioned
     * @ORM\Column(name="street", type="string", length=255)
     */
    private $street;

    /**
     * @var string
     *@Gedmo\Versioned
     * @ORM\Column(name="cnr_street", type="string", length=255)
     */
    private $cnrStreet;

    /**
     * @var string
     *@Gedmo\Versioned
     * @ORM\Column(name="gps_long", type="string", length=255)
     */
    private $gpsLong;

    /**
     * @var string
     * @Gedmo\Versioned
     * @ORM\Column(name="gps_lat", type="string", length=255)
     */
    private $gpsLat;

    /**
     * @var string
     * @Gedmo\Versioned
     * @ORM\Column(name="maplocation", type="string", length=255,nullable=true)
     */
    private $mapLocation;

    /**
     * @var integer
     *      @Gedmo\Versioned
     * @ORM\ManyToOne(targetEntity="SB\BillBoardBundle\Entity\Illumination", inversedBy="product")
     * @ORM\JoinColumn(name="illumination_id", referencedColumnName="id")
     *
     */
    private $illuminationId;

    /**
     * @var integer
     * @Gedmo\Versioned
     * @ORM\ManyToOne(targetEntity="SB\BillBoardBundle\Entity\Type", inversedBy="product")
     * @ORM\JoinColumn(name="type_id", referencedColumnName="id")
     */
    private $typeId;

    /**
     * @var integer
     * @Gedmo\Versioned
     * @ORM\ManyToOne(targetEntity="SB\BillBoardBundle\Entity\Size", inversedBy="product")
     * @ORM\JoinColumn(name="size_id", referencedColumnName="id")
     */
    private $sizeId;

    /**
     * @var integer
     * @Gedmo\Versioned
     * @ORM\ManyToOne(targetEntity="SB\BillBoardBundle\Entity\Category", inversedBy="product")
     * @ORM\JoinColumn(name="cateogry_id", referencedColumnName="id")
     */
    private $categoryId;

    /**
     * @var integer
     * @Gedmo\Versioned
     * @ORM\ManyToOne(targetEntity="SB\BillBoardBundle\Entity\RateType", inversedBy="product")
     * @ORM\JoinColumn(name="rate_type_id", referencedColumnName="id")
     */
    private $rateTypeId;

    /**
     * @var integer
     *
     * @Gedmo\Versioned
     * @ORM\ManyToOne(targetEntity="SB\BillBoardBundle\Entity\DisplayTemplate", inversedBy="product")
     * @ORM\JoinColumn(name="display_template_id", referencedColumnName="id")
     */
    private $displayTemplateId;

     /**
    * @ORM\Column(name="deletedAt", type="datetime", nullable=true)
    */
    private $deletedAt;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Product
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get map location
     *
     * @return string
     */
    public function getMapLocation()
    {
        return $this->mapLocation;
    }
    /**
     * Set map location
     *
     * @param string $mapLocation
     * @return mapLocation
     */
    public function setMapLocation($mapLocation)
    {
        $this->mapLocation = $mapLocation;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    /**
     * Set number
     *
     * @param string $number
     * @return Product
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set siteNumber
     *
     * @param string $siteNumber
     * @return Product
     */
    public function setSiteNumber($siteNumber)
    {
        $this->siteNumber = $siteNumber;

        return $this;
    }

    /**
     * Get siteNumber
     *
     * @return string
     */
    public function getSiteNumber()
    {
        return $this->siteNumber;
    }

    /**
     * Set provinceId
     *
     * @param integer $provinceId
     * @return Product
     */
    public function setProvinceId($provinceId)
    {
        $this->provinceId = $provinceId;

        return $this;
    }

    /**
     * Get provinceId
     *
     * @return integer
     */
    public function getProvinceId()
    {
        return $this->provinceId;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return Product
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set suburb
     *
     * @param string $suburb
     * @return Product
     */
    public function setSuburb($suburb)
    {
        $this->suburb = $suburb;

        return $this;
    }

    /**
     * Get suburb
     *
     * @return string
     */
    public function getSuburb()
    {
        return $this->suburb;
    }

    /**
     * Set street
     *
     * @param string $street
     * @return Product
     */
    public function setStreet($street)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * Get street
     *
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Set cnrStreet
     *
     * @param string $cnrStreet
     * @return Product
     */
    public function setCnrStreet($cnrStreet)
    {
        $this->cnrStreet = $cnrStreet;

        return $this;
    }

    /**
     * Get cnrStreet
     *
     * @return string
     */
    public function getCnrStreet()
    {
        return $this->cnrStreet;
    }

    /**
     * Set gpsLong
     *
     * @param string $gpsLong
     * @return Product
     */
    public function setGpsLong($gpsLong)
    {
        $this->gpsLong = $gpsLong;

        return $this;
    }

    /**
     * Get gpsLong
     *
     * @return string
     */
    public function getGpsLong()
    {
        return $this->gpsLong;
    }

    /**
     * Set gpsLat
     *
     * @param string $gpsLat
     * @return Product
     */
    public function setGpsLat($gpsLat)
    {
        $this->gpsLat = $gpsLat;

        return $this;
    }

    /**
     * Get gpsLat
     *
     * @return string
     */
    public function getGpsLat()
    {
        return $this->gpsLat;
    }

    /**
     * Set illuminationId
     *
     * @param integer $illuminationId
     * @return Product
     */
    public function setIlluminationId($illuminationId)
    {
        $this->illuminationId = $illuminationId;

        return $this;
    }

    /**
     * Get illuminationId
     *
     * @return integer
     */
    public function getIlluminationId()
    {
        return $this->illuminationId;
    }

    /**
     * Set typeId
     *
     * @param integer $typeId
     * @return Product
     */
    public function setTypeId($typeId)
    {
        $this->typeId = $typeId;

        return $this;
    }

    /**
     * Get typeId
     *
     * @return integer
     */
    public function getTypeId()
    {
        return $this->typeId;
    }

    /**
     * Set sizeId
     *
     * @param integer $sizeId
     * @return Product
     */
    public function setSizeId($sizeId)
    {
        $this->sizeId = $sizeId;

        return $this;
    }

    /**
     * Get sizeId
     *
     * @return integer
     */
    public function getSizeId()
    {
        return $this->sizeId;
    }

    /**
     * Set categoryId
     *
     * @param integer $categoryId
     * @return Product
     */
    public function setCategoryId($categoryId)
    {
        $this->categoryId = $categoryId;

        return $this;
    }

    /**
     * Get categoryId
     *
     * @return integer
     */
    public function getCategoryId()
    {
        return $this->categoryId;
    }

    /**
     * Set rateType
     *
     * @param string $rateType
     * @return Product
     */
    public function setRateTypeId($rateTypeId)
    {
        $this->rateTypeId = $rateTypeId;

        return $this;
    }

    /**
     * Get rateType
     *
     * @return string
     */
    public function getRateTypeId()
    {
        return $this->rateTypeId;
    }

    /**
     * Set displayTemplateId
     *
     * @param integer $displayTemplateId
     * @return Product
     */
    public function setDisplayTemplateId($displayTemplateId)
    {
        $this->displayTemplateId = $displayTemplateId;

        return $this;
    }

    /**
     * Get templateId
     *
     * @return integer
     */
    public function getDisplayTemplateId()
    {
        return $this->displayTemplateId;
    }

    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;
    }

    public function __construct()
    {
        $this->product = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->getName();
    }

	public $images;

    public function setImages($images = null)
    {
        $this->images = $images;
    }

    public function getImages()
    {
        return $this->images;
    }
	/**
     * @var string
     * @Gedmo\Versioned
     * @ORM\Column(name="price", type="string", length=255)
     */
    public $price;

    public function setPrice($price = null)
    {
        $this->price = $price;
    }

    public function getPrice()
    {
        return $this->price;
    }

}
