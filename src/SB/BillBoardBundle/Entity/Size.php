<?php

namespace SB\BillBoardBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Size
 *
 * @ORM\Table(name="size")
 * @ORM\Entity
 * @ORM\Table(name="size",indexes={
 *           @ORM\Index(name="size", columns={"size"})
 *      }
 * ) 
 * 
 * @Gedmo\Loggable()
 * @Gedmo\SoftDeleteable(fieldName="deletedAt")
 */
class Size
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Gedmo\Versioned
     * @ORM\Column(name="size", type="string", length=255)
     */
    private $size;
    
    /**
    * @ORM\Column(name="deletedAt", type="datetime", nullable=true)
    */
    private $deletedAt;
    
    /**
     * @ORM\OneToMany(targetEntity="SB\BillBoardBundle\Entity\Product", mappedBy="size")
     */
    protected $product;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set size
     *
     * @param string $size
     * @return Size
     */
    public function setSize($size)
    {
        $this->size = $size;

        return $this;
    }

    /**
     * Get size
     *
     * @return string 
     */
    public function getSize()
    {
        return $this->size;
    }
    
     public function __construct()
    {
        $this->size = new ArrayCollection();
    }

    public function __toString()
    {
         return $this->getSize();
    }
    
    /**
     * Add example
     *
     * @param \SB\BillBoardBundle\Entity\Product $product
     * @return Colour
     */
    public function addProduct(\SB\BillBoardBundle\Entity\Product $product)
    {
        $this->product[] = $product;
    
        return $this;
    }

    /**
     * Remove example
     *
     * @param \SB\BillBoardBundle\Entity\Product $product
     */
    public function removeProduct(\SB\BillBoardBundle\Entity\Product $product)
    {
        $this->product->removeElement($product);
    }

    
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;
    }
}
