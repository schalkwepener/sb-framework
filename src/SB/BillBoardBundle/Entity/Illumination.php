<?php

namespace SB\BillBoardBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Illumination
 *
 * @ORM\Table(name="illumination")
 * @ORM\Entity
 * 
 * @ORM\Table(name="illumination",indexes={
 *           @ORM\Index(name="illumination", columns={"illumination_type"})
 *      }
 * ) 
 * 
 * @Gedmo\Loggable()
 * @Gedmo\SoftDeleteable(fieldName="deletedAt")
 */
class Illumination
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Gedmo\Versioned
     * @ORM\Column(name="illumination_type", type="string", length=255)
     */
    private $illuminationType;

    /**
    * @ORM\Column(name="deletedAt", type="datetime", nullable=true)
    */
    private $deletedAt;
    
    /**
     * @ORM\OneToMany(targetEntity="SB\BillBoardBundle\Entity\Product", mappedBy="category")
     */
    protected $product;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set illuminationType
     *
     * @param string $illuminationType
     * @return Illumination
     */
    public function setIlluminationType($illuminationType)
    {
        $this->illuminationType = $illuminationType;

        return $this;
    }

    /**
     * Get illuminationType
     *
     * @return string 
     */
    public function getIlluminationType()
    {
        return $this->illuminationType;
    }
    
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;
    }
    
     public function __construct()
    {
        $this->category = new ArrayCollection();
    }

    public function __toString()
    {
         return $this->getIlluminationType();
    }
    
    /**
     * Add example
     *
     * @param \SB\BillBoardBundle\Entity\Product $product
     * @return Colour
     */
    public function addProduct(\SB\BillBoardBundle\Entity\Product $product)
    {
        $this->product[] = $product;
    
        return $this;
    }

    /**
     * Remove example
     *
     * @param \SB\BillBoardBundle\Entity\Product $product
     */
    public function removeProduct(\SB\BillBoardBundle\Entity\Product $product)
    {
        $this->product->removeElement($product);
    }

}
