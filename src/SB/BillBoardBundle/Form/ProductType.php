<?php

namespace SB\BillBoardBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

//use SB\UploaderBundle\Form\Type\UploaderType;

class ProductType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('number')
            ->add('siteNumber')
            ->add('provinceId')
            ->add('city')
           // ->add('suburb')
            ->add('street')
            ->add('cnrStreet')
            ->add('gpsLong')
            ->add('gpsLat')
            ->add('illuminationId')
            ->add('typeId')
            ->add('sizeId')
            ->add('categoryId')
            ->add('rateTypeId')
            ->add('displayTemplateId')
            ->add('deletedAt')
        ;
      //  $builder->add('uploader', 'custom_uploader');
        $builder->add('mapLocation', 'googlemap_type');
		$builder->add('images', 'multiuploader', array());
//        $builder->add('suburb', 'tel', array(
//            'label' => 'Phone Number',
//        ));
    }


    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'SB\BillBoardBundle\Entity\Product'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'sb_billboardbundle_product';
    }
}
