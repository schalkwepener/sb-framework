<?php
// src/Acme/DemoBundle/Form/Type/GenderType.php

namespace SB\BillBoardBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class BillBoardSearchType extends AbstractType
{
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
           /* 'choices' => array(
                'm' => 'Male',
                'f' => 'Female',
            )*/
            'text' => 'woof',
            'label' => 'Registration Number 1',
            'widget' => 'billboard_search_type',
            'data_class' => null
        ));
    }

    public function getParent()
    {
        return 'text';
    }

    public function getName()
    {
        return 'billboard_search_type';
    }
}
?>