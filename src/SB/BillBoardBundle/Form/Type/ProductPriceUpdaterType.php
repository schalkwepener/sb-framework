<?php
// src/Acme/DemoBundle/Form/Type/GenderType.php

namespace SB\BillBoardBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ProductPriceUpdaterType extends AbstractType
{
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
           /* 'choices' => array(
                'm' => 'Male',
                'f' => 'Female',
            )*/
            'text' => 'woof',
            'label' => 'Registration Number 1',
            'widget' => 'product_price_updater_type',
            'data_class' => null
        ));
    }

    public function getParent()
    {
        return 'text';
    }

    public function getName()
    {
        return 'product_price_updater_type';
    }
}
?>