$(document).ready(function(){

    $(".check_all").click(function() {       
        $(this).closest('.records_list').find('input[type=checkbox]').prop('checked', this.checked);        
    });
    
     $(".check_all").click(function() {       
        $(this).closest('.records_list').find('input[type=checkbox]').prop('checked', this.checked);        
    });
    
    $('.radio_update').on('click', function () {
        var selection = $(this).val();
        if(selection == 'yes'){
            $(".price_controls").show();
        }else{
            $(".price_controls").hide();
        }
    });
     
    $(".billboard-export").click(function(event) { 
        event.preventDefault();
       
        var allVals = '';
        var _this = event.target;  
        
        $('#product_rows .export').each(function(element) {
            var shit = '#'+this.id;

            if($(shit).is(':checked')){
              allVals = allVals + "_" + this.value;
            }
        });
        
        var url = _this.dataset.url;
        url = url + "?products="+allVals;
       
        $('#alertTop').html('Exporting');
        $('#alertTop').addClass("alert-success");
        $('#alertTop').show();
       
       var newWindow = window.open(url, 'get your pptx here', "_blank");
         /*    setTimeout(
                function()
                {
                  newWindow.close();
                },
                5000
               );*/
        $('#alertTop').hide();    
        $('#alertTop').html('Did you get it?');
        $('#alertTop').addClass("alert-info");
        $('#alertTop').show();       
    });
    
    
    
    $(".billboard-search").click(function(event) {
     
        $('#alertTop').hide();
        var _this = event.target;  
        
        $.ajax({
            type: "POST",
            url: _this.dataset.url,
            data: {
                province: $("#province").val(),
                type: $("#type").val(),
                category: $("#category").val()
            },
            success: function(dataCheck) { 
                $(".records_list").remove();
                $("#product_rows").append(dataCheck);
                /*alert(dataCheck);
                
                if (dataCheck == 'Done') { 
                   var row_id = "#"+_this.id;
                   
                    $(row_id).hide();
                   
                   $('#alertTop').html('This the search resutls');
                   $('#alertTop').addClass("alert-success");
                   $('#alertTop').show();
                    
                }*/
            }
        });
    });
    
    
    $(".billboard-price-update").click(function(event) { 
        $('#alertTop').hide();
        var _this = event.target;   
        var allVals = ''; 
        
        $('#product_rows .export').each(function(element) {
            var shit = '#'+this.id;

            if($(shit).is(':checked')){
              allVals = allVals + "_" + this.value;
            }
        });
        
        var url = _this.dataset.url;
        url = url + "?products="+allVals; 
        
        var direction = $('.radio_direction:checked').val(); 
        
        var modifier_fixed = $('.amount_fixed').val();
        var modifier_percentage = $('.amount_percentage').val();
        var modify_by_val = 0;
        var modifier = '';
       
        if(modifier_fixed == ''){
            modify_by_val = modifier_percentage;     
            modifier = 'percentage';
        }else{
            modify_by_val = modifier_fixed;   
            modifier = 'fixed';
        }
          
        $.ajax({
            type: "POST",
            url:  url,
            data: {
                direction: direction,
                modify_by_val: modify_by_val,
                modifier: modifier,
                province: $("#province").val(),
                type: $("#type").val(),
                category: $("#category").val()
            },
            success: function(dataCheck) { 
                //$(".records_list").remove();
                $(".records_list").empty();
                $("#product_rows").append(dataCheck);
               
                $('#alertTop').html('Prices updated');
                $('#alertTop').addClass("alert-success");
                $('#alertTop').show();
               
               /*alert(dataCheck);                
                if (dataCheck == 'Done') { 
                   var row_id = "#"+_this.id;
                   
                    $(row_id).hide(); 
              }*/
            }
        });
    });
});

