<?php

namespace SB\OfficeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/office")
     * @Method("GET")
     * @Template()
     */
    public function indexAction($name='office')
    {     
        $phpPowerPoint = $this->get('phppowerpoint')->createPHPPowerPointObject();     
     
        $phpPowerPoint->getProperties()->setCreator('Maarten Balliauw')
            ->setLastModifiedBy('Maarten Balliauw')
            ->setTitle('Office 2007 PPTX Test Document')
            ->setSubject('Office 2007 PPTX Test Document')
            ->setDescription('Test document for Office 2007 PPTX, generated using PHP classes.')
            ->setKeywords('office 2007 openxml php')
            ->setCategory('Test result file');
      
        $currentSlide = $phpPowerPoint->getActiveSlide();
        
        $shape = $currentSlide->createRichTextShape()
                ->setHeight(300)
                ->setWidth(600)
                ->setOffsetX(170)
                ->setOffsetY(180);
           
        $shape->getActiveParagraph()->getAlignment()->setHorizontal( $shape->getActiveParagraph()->getAlignment()->getHorizontal() ); 
           
        $textRun = $shape->createTextRun( 'Noel SUCKS BIG BLACK BALLS!');
      
        
        //$arr = get_declared_classes();
        $color = new \PHPPowerPoint_Style_Color();
        $color->setARGB('FFCBDB'); 
        
        $textRun->getFont()->setBold(true)
            ->setSize(60)
            ->setColor( $color )
            ;
                 
                $basename = "././".basename(__FILE__, '.php');
              
                
                $basename = "test";
                
                $extension = 'pptx';
                $format = 'PowerPoint2007';
                $objWriter = $this->get('phppowerpoint')->createWriter($phpPowerPoint,$format);
                
                $objWriter->save("{$basename}.{$extension}");
               
               $response = $this->get('phppowerpoint')->createStreamedResponse($objWriter);
               $response->headers->set('Content-Type', 'application/vnd.ms-powerpoint; charset=utf-8');
               $response->headers->set('Content-Disposition', 'attachment;filename=stream-file.pptx');
               $response->headers->set('Pragma', 'public');
               $response->headers->set('Cache-Control', 'maxage=1');
              
        return $response;     
       
      
    }
    
    /**
     * @Route("/office/export", name="export")
     * @Method("GET")
     */
    public function exportAction(Request $request){
        
        $em = $this->getDoctrine()->getManager();
        $connection = $em->getConnection();  
       
        $all = $_REQUEST['products'];
        $all_array = explode("_",$all);
        $all_array_clean = array_filter($all_array);      
        
        $phpPowerPoint = $this->get('phppower')->createPHPPowerPointObject();
       
        $currentSlide = $phpPowerPoint->getActiveSlide();
        $shape = $currentSlide->createRichTextShape()
            ->setHeight(400)
            ->setWidth(600)
            ->setOffsetX(170)
            ->setOffsetY(180)
            ->setInsetTop(50)
            ->setInsetBottom(50);
         
        $shape->getActiveParagraph()->getAlignment()->setHorizontal( $shape->getActiveParagraph()->getAlignment()->getHorizontal() ); 
                        
        $color = new \PHPPowerPoint_Style_Color();
        $color->setARGB('FFCBDB'); 
            
        $textRun = $shape->createTextRun('Thank you for using PHPPowerPoint!');
        $textRun->getFont()->setBold(true)
            ->setSize(60)
            ->setColor( $color );   
        
        foreach($all_array_clean as $id){ 
            
            $entity = $em->getRepository('SBBillBoardBundle:Product')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Product entity.');
            }
        
        
            $slide2 = $phpPowerPoint->createSlide();
            $shape2 = $slide2->createRichTextShape()
                    ->setHeight(400)
                    ->setWidth(600)
                    ->setOffsetX(170)
                    ->setOffsetY(180)
                    ->setInsetTop(50)
                    ->setInsetBottom(50); 

            $shape2->getActiveParagraph()->getAlignment()->setHorizontal( $shape2->getActiveParagraph()->getAlignment()->getHorizontal() ); 

            $textRun2 = $shape2->createTextRun($id);
            $textRun2->getFont()->setBold(true)
                    ->setSize(60)
                    ->setColor( $color );
         }
         $basename = "test";
                
        $extension = 'pptx';
        $format = 'PowerPoint2007';
        $objWriter = $this->get('phppower')->createWriter($phpPowerPoint,$format);
        
        $response = $this->get('phppower')->createStreamedResponse($objWriter);
        $response->headers->set('Content-Type', 'application/vnd.ms-powerpoint; charset=utf-8');
        $response->headers->set('Content-Disposition', 'attachment;filename=stream-file.pptx');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');
       
        return $response;
    }
    
    /**
     * @Route("/office/template")
     * @Template()
     */
    
     public function templateAction($name='template')
        { 

            $phpPowerPoint = $this->get('phppowerpoint')->createPHPPowerPointObject();     
       
            $currentSlide = $phpPowerPoint->getActiveSlide();
            
            $shape = $currentSlide->createDrawingShape();
            
            $shape->setName('PHPPowerPoint logo')
                  ->setDescription('PHPPowerPoint logo')
                  ->setPath('././phppowerpoint_logo.gif')
                  ->setHeight(36)
                  ->setOffsetX(10)
                  ->setOffsetY(10);
           
            $shape->getShadow()->setVisible(true)
                               ->setDirection(45)
                               ->setDistance(10);
                        
            $shape = $currentSlide->createRichTextShape()
                ->setHeight(400)
                ->setWidth(600)
                ->setOffsetX(170)
                ->setOffsetY(180)
                ->setInsetTop(50)
                ->setInsetBottom(50);
         
            $shape->getActiveParagraph()->getAlignment()->setHorizontal( $shape->getActiveParagraph()->getAlignment()->getHorizontal() ); 
                        
            $color = new \PHPPowerPoint_Style_Color();
            $color->setARGB('FFCBDB'); 
            
            $textRun = $shape->createTextRun('Thank you for using PHPPowerPoint!');
            $textRun->getFont()->setBold(true)
                               ->setSize(60)
                               ->setColor( $color ) ;
            
            $shape->getHyperlink()->setUrl("http://phppowerpoint.codeplex.com")
                                  ->setTooltip('PHPPowerPoint');
            
            $shape = $currentSlide->createLineShape(170, 180, 770, 180);
            $shape->getBorder()->getColor()->setARGB('FFC00000');
            
            $shape = $currentSlide->createLineShape(170, 580, 770, 580);
            $shape->getBorder()->getColor()->setARGB('FFC00000');
            
            $basename = "././".basename(__FILE__, '.php');
            $extension = 'pptx';
            $format = 'PowerPoint2007';
            $objWriter = $this->get('phppowerpoint')->createWriter($phpPowerPoint,$format);
            
            $layout = new \PHPPowerPoint_Writer_PowerPoint2007_LayoutPack_TemplateBased('././template.pptx');
            $objWriter->setLayoutPack( $layout); 
            
            $basename = "././".basename(__FILE__, '.php');
            $extension = 'pptx';
            $format = 'PowerPoint2007'; 
            $objWriter->save("{$basename}.{$extension}");

           $response = $this->get('phppowerpoint')->createStreamedResponse($objWriter);
           $response->headers->set('Content-Type', 'application/vnd.ms-powerpoint; charset=utf-8');
           $response->headers->set('Content-Disposition', 'attachment;filename=stream-file.pptx');
           $response->headers->set('Pragma', 'public');
           $response->headers->set('Cache-Control', 'maxage=1');
           
           return $response;
    } 
    
    /**
     * @Route("/office/multiple")
     * @Template()
     */
    
     public function multipleAction($name='multiple')
    { 
        
            $phpPowerPoint = $this->get('phppowerpoint')->createPHPPowerPointObject();     
       
            $currentSlide = $phpPowerPoint->getActiveSlide();
            
            $shape = $currentSlide->createDrawingShape();
            
            $shape->setName('PHPPowerPoint logo')
                  ->setDescription('PHPPowerPoint logo')
                  ->setPath('././phppowerpoint_logo.gif')
                  ->setHeight(36)
                  ->setOffsetX(10)
                  ->setOffsetY(10);
           
            $shape->getShadow()->setVisible(true)
                               ->setDirection(45)
                               ->setDistance(100);
         
            $slide2 = $phpPowerPoint->createSlide();

            // Add background image
            $shape2 = $slide2->createDrawingShape();
            $shape2->setName('Background');
            $shape2->setDescription('Background');
            $shape2->setPath('././realdolmen_bg.jpg');
            $shape2->setWidth(950);
            $shape2->setHeight(720);
            $shape2->setOffsetX(0);
            $shape2->setOffsetY(0);
         
            $basename = "././".basename(__FILE__, '.php');
            $extension = 'pptx';
            $format = 'PowerPoint2007';
            $objWriter = $this->get('phppowerpoint')->createWriter($phpPowerPoint,$format);
            
            $layout = new \PHPPowerPoint_Writer_PowerPoint2007_LayoutPack_TemplateBased('././template.pptx');
            $objWriter->setLayoutPack( $layout); 
            
            $basename = "././".basename(__FILE__, '.php');
            $extension = 'pptx';
            $format = 'PowerPoint2007'; 
            $objWriter->save("{$basename}.{$extension}");

           $response = $this->get('phppowerpoint')->createStreamedResponse($objWriter);
           $response->headers->set('Content-Type', 'application/vnd.ms-powerpoint; charset=utf-8');
           $response->headers->set('Content-Disposition', 'attachment;filename=stream-file.pptx');
           $response->headers->set('Pragma', 'public');
           $response->headers->set('Cache-Control', 'maxage=1');
           
           return $response;
    } 
    
    
     /**
     * @Route("/office/reader")
     * @Template()
     */
    
     public function readerAction($name='reader')
    { 
                
         $phpPowerPoint = $this->get('phppowerpoint')->createPHPPowerPointObject();    
         $phpPowerPoint->getProperties()->setCreator('Maarten Balliauw')
            ->setLastModifiedBy('Maarten Balliauw')
            ->setTitle('Office 2007 PPTX Test Document')
            ->setSubject('Office 2007 PPTX Test Document')
            ->setDescription('Test document for Office 2007 PPTX, generated using PHP classes.')
            ->setKeywords('office 2007 openxml php')
            ->setCategory('Test result file');
         
            $currentSlide = $phpPowerPoint->getActiveSlide();
            $slide2 = $phpPowerPoint->createSlide();

            // Add background image
          
            /* 
            $shape2 = $slide2->createDrawingShape();
            $shape2->setName('Background');
            $shape2->setDescription('Background');
            $shape2->setPath('././realdolmen_bg.jpg');
            $shape2->setWidth(950);
            $shape2->setHeight(720);
            $shape2->setOffsetX(0);
            $shape2->setOffsetY(0); 
            */
         
            $basename = "././".basename(__FILE__, '.php');
            $extension = 'pptx';
            $format = 'PowerPoint2007';
            $objWriter = $this->get('phppowerpoint')->createWriter($phpPowerPoint,$format);
            
            $layout = new \PHPPowerPoint_Writer_PowerPoint2007_LayoutPack_TemplateBased('././fuck2.potx');
            
            $objWriter->setLayoutPack($layout); 
          
            $currentSlide = $phpPowerPoint->getActiveSlide();
        
            $currentSlide = $phpPowerPoint->getActiveSlide();
           
            
           
            $objWriter->save("{$basename}.{$extension}");

            $response = $this->get('phppowerpoint')->createStreamedResponse($objWriter);
            $response->headers->set('Content-Type', 'application/vnd.ms-powerpoint; charset=utf-8');
            $response->headers->set('Content-Disposition', 'attachment;filename=readthis-mod1.pptx');
            $response->headers->set('Pragma', 'public');
            $response->headers->set('Cache-Control', 'maxage=1');       
       }
}
