<?php

namespace SB\PriceUpdaterBundle\Service;

use Symfony\Component\HttpFoundation\Request;

use SB\PriceUpdaterBundle\Entity\PriceReport;

use Doctrine\DBAL\Connection;

/**
 * Emails controller.
 *
 * @Route("/pricereports")
 */
class PriceReportService 
{

    private $connection;
    private $em;
   
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        $this->em = $em;
    }
    
    /**
     * Lists all Emails entities.
     *
     * @Route("/", name="emails123")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
     /*   $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('WBCEmailCronBundle:Emails')->findAll();

        return array(
            'entities' => $entities,
        );*/
    }
    
    public function createPriceReportAction($productId,$movementDirection,$movementMethod,$movementValue,$userId)
    {

        $entity = new PriceReport();
        
        $transactionDate = new \DateTime('now');
        
        $entity->setProductId($this->em->getRepository('SBBillBoardBundle:Product')->findOneById($productId));
        $entity->setMovementDirection($movementDirection);
        $entity->setMovementMethod($movementMethod);
        $entity->setMovementValue($movementValue);
        $entity->setTransactionDate($transactionDate);
        //$entity->setTransactionDate($userId);
		
        $this->em->persist($entity);
        $this->em->flush();  
      
        return $entity->getId();
    }
}

?>
