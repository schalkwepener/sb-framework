<?php

namespace SB\PriceUpdaterBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use SB\PriceUpdaterBundle\Entity\PriceReport;
use SB\PriceUpdaterBundle\Form\PriceReportType;

/**
 * PriceReport controller.
 *
 * @Route("/pricereport")
 */
class PriceReportController extends Controller
{

    /**
     * Lists all PriceReport entities.
     *
     * @Route("/", name="pricereport")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('SBPriceUpdaterBundle:PriceReport')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new PriceReport entity.
     *
     * @Route("/", name="pricereport_create")
     * @Method("POST")
     * @Template("SBPriceUpdaterBundle:PriceReport:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new PriceReport();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('pricereport_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
    * Creates a form to create a PriceReport entity.
    *
    * @param PriceReport $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(PriceReport $entity)
    {
        $form = $this->createForm(new PriceReportType(), $entity, array(
            'action' => $this->generateUrl('pricereport_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new PriceReport entity.
     *
     * @Route("/new", name="pricereport_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new PriceReport();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a PriceReport entity.
     *
     * @Route("/{id}", name="pricereport_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SBPriceUpdaterBundle:PriceReport')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find PriceReport entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing PriceReport entity.
     *
     * @Route("/{id}/edit", name="pricereport_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SBPriceUpdaterBundle:PriceReport')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find PriceReport entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a PriceReport entity.
    *
    * @param PriceReport $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(PriceReport $entity)
    {
        $form = $this->createForm(new PriceReportType(), $entity, array(
            'action' => $this->generateUrl('pricereport_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing PriceReport entity.
     *
     * @Route("/{id}", name="pricereport_update")
     * @Method("PUT")
     * @Template("SBPriceUpdaterBundle:PriceReport:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SBPriceUpdaterBundle:PriceReport')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find PriceReport entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('pricereport_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a PriceReport entity.
     *
     * @Route("/{id}", name="pricereport_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('SBPriceUpdaterBundle:PriceReport')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find PriceReport entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('pricereport'));
    }

    /**
     * Creates a form to delete a PriceReport entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('pricereport_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
    
    /**
     * Edits an existing PriceReport entity.
     *
     * @Route("/save", name="pricereport_save")
     * @Method("PUT")
     *  
     */
    public function saveReportAction($productId,$movementDirection,$movementMethod,$movementValue,$transactionDate,$userId)
    {
        
        $entity = new PriceReport();
        
        $em = $this->getDoctrine()->getManager();
        
        $entity->setProductId($productId);
        $entity->setMovementDirection($movementDirection);
        $entity->setMovementMethod($movementMethod);
        $entity->setMovementValue($movementValue);
        $entity->setTransactionDate($transactionDate);
        //$entity->setUserId($user);
        
        $em->persist($entity);
        $em->flush();
        
        return true;
    }
}
