<?php

namespace SB\PriceUpdaterBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * PriceReport
 *
 * @ORM\Table(name="pricereport")
 * @ORM\Entity
 * 
 * @Gedmo\Loggable()
 * @Gedmo\SoftDeleteable(fieldName="deletedAt")
 */
class PriceReport
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="transaction_date", type="datetime")
     */
    private $transactionDate;

    /**
     * @var integer
     * 
     * @Gedmo\Versioned
     * @ORM\ManyToOne(targetEntity="SB\BillBoardBundle\Entity\Product", inversedBy="pricereport")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     */
    private $productId;

    /**
     * @var string
     *
     * @ORM\Column(name="movement_value", type="string", length=255)
     */
    private $movementValue;

    /**
     * @var string
     *
     * @ORM\Column(name="movement_method", type="string", length=255)
     */
    private $movementMethod;

    /**
     * @var string
     *
     * @ORM\Column(name="movement_direction", type="string", length=255)
     */
    private $movementDirection;

    /**
    * @ORM\Column(name="deletedAt", type="datetime", nullable=true)
    */
    private $deletedAt;
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set transactionDate
     *
     * @param \DateTime $transactionDate
     * @return PriceReport
     */
    public function setTransactionDate($transactionDate)
    {
        $this->transactionDate = $transactionDate;

        return $this;
    }

    /**
     * Get transactionDate
     *
     * @return \DateTime 
     */
    public function getTransactionDate()
    {
        return $this->transactionDate;
    }

    /**
     * Set productId
     *
     * @param integer $productId
     * @return PriceReport
     */
    public function setProductId($productId)
    {
        $this->productId = $productId;

        return $this;
    }

    /**
     * Get productId
     *
     * @return integer 
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * Set movementValue
     *
     * @param string $movementValue
     * @return PriceReport
     */
    public function setMovementValue($movementValue)
    {
        $this->movementValue = $movementValue;

        return $this;
    }

    /**
     * Get movementValue
     *
     * @return string 
     */
    public function getMovementValue()
    {
        return $this->movementValue;
    }

    /**
     * Set movementMethod
     *
     * @param string $movementMethod
     * @return PriceReport
     */
    public function setMovementMethod($movementMethod)
    {
        $this->movementMethod = $movementMethod;

        return $this;
    }

    /**
     * Get movementMethod
     *
     * @return string 
     */
    public function getMovementMethod()
    {
        return $this->movementMethod;
    }

    /**
     * Set movementDirection
     *
     * @param string $movementDirection
     * @return PriceReport
     */
    public function setMovementDirection($movementDirection)
    {
        $this->movementDirection = $movementDirection;

        return $this;
    }

    /**
     * Get movementDirection
     *
     * @return string 
     */
    public function getMovementDirection()
    {
        return $this->movementDirection;
    }
    
    public function __construct()
    {
        $this->priceReport = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->getId();
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     * @return PriceReport
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime 
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }
}
