<?php

namespace SB\PriceUpdaterBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PriceReportType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('transactionDate')
            ->add('movementValue')
            ->add('movementMethod')
            ->add('movementDirection')
            ->add('deletedAt')
            ->add('productId')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'SB\PriceUpdaterBundle\Entity\PriceReport'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'sb_priceupdaterbundle_pricereport';
    }
}
