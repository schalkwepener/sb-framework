/* src/SB/SecurityBundle/Resources/public/js/login.js */
(function ($) {

	var login = function(options){
		var lgn = this;
		lgn.userInput;
		lgn.passInput;
		lgn.modalEl = 'login-error-modal';

		__construct = function(that) {
			$.extend(that, options);
		}(lgn);

	};
	login.prototype = {
//		init : function(){
//			this.createModal();
//		},
		createModal : function(elId){
			var _this = this;
			elId = elId || _this.modalEl;
			$('#' + elId).dialog({
				modal: true,
				draggable:true,
				height:'auto'
			});
		}
	};
	namespace("sbf.security.login",new login);

})(jQuery);
