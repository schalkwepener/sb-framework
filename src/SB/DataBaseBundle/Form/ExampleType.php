<?php

namespace SB\DataBaseBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ExampleType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('number')
          //  ->add('deletedAt')
          //  ->add('createdBy')
          //  ->add('updatedBy') 
             ->add('colour_id')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'SB\DataBaseBundle\Entity\Example'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'sb_databasebundle_example';
    }
}
