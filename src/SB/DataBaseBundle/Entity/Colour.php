<?php

namespace SB\DataBaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Colour
 *
 * @ORM\Table(name="colour")
 * @ORM\Entity
 * @Gedmo\Loggable()
 * @Gedmo\SoftDeleteable(fieldName="deletedAt")
 */
class Colour
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Gedmo\Versioned
     * @ORM\Column(name="colour", type="string", length=255)
     */
    private $colour;
    
    /**
    * @ORM\OneToMany(targetEntity="Example", mappedBy="colour")
    */
    protected $example;
    
     /**
     * @ORM\Column(name="deletedAt", type="datetime", nullable=true)
     */
    private $deletedAt;
    
    public function __construct()
    {
        $this->example = new ArrayCollection();
    }

    public function __toString()
    {
         return $this->getColour();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set colour
     *
     * @param string $colour
     * @return Colour
     */
    public function setColour($colour)
    {
        $this->colour = $colour;

        return $this;
    }

    /**
     * Get colour
     *
     * @return string 
     */
    public function getColour()
    {
        return $this->colour;
    }
    
    /**
     * Add example
     *
     * @param \SB\DataBaseBundle\Entity\Example $example
     * @return Colour
     */
    public function addExample(\SB\DataBaseBundle\Entity\Example $example)
    {
        $this->example[] = $example;
    
        return $this;
    }

    /**
     * Remove example
     *
     * @param \SB\DataBaseBundle\Entity\Example $example
     */
    public function removeExample(\SB\DataBaseBundle\Entity\Example $example)
    {
        $this->example->removeElement($example);
    }

    /**
     * Get example
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getExample()
    {
        return $this->example;
    }
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;
    }
}
