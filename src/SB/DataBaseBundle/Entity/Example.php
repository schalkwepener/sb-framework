<?php

namespace SB\DataBaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Example
 *
 * @ORM\Table(name="example")
 * @ORM\Entity
 * @ORM\Table(name="example",indexes={
 *           @ORM\Index(name="example_numbers", columns={"name", "number"}),
 *           @ORM\Index(name="example_id", columns={"id"})
 *      }
 * ) 
 * 
 * @Gedmo\Loggable()
 * @Gedmo\SoftDeleteable(fieldName="deletedAt")
 *  
 */
class Example
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Gedmo\Versioned
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var integer
     * @Gedmo\Versioned
     * @ORM\Column(name="number", type="integer")
     */
    private $number;
    
     /**
     * @ORM\Column(name="deletedAt", type="datetime", nullable=true)
     */
    private $deletedAt;
     
    /**
    * 
    * @Gedmo\Versioned
    * @ORM\ManyToOne(targetEntity="Colour", inversedBy="example")
    * @ORM\JoinColumn(name="colour_id", referencedColumnName="id")
    */
    private $colour_id;
	
    /**
     * @ORM\OneToMany(targetEntity="SB\DataBaseBundle\Entity\Invoice", mappedBy="example")
     */
    protected $invoice;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Example
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set number
     *
     * @param integer $number
     * @return Example
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return integer 
     */
    public function getNumber()
    {
        return $this->number;
    }
    
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;
    }
    
     /**
     * Set colour_id
     *
     * @param \SB\DataBaseBundle\Entity\colour $colour_id
     * @return Example
     */
    public function setColourId(\SB\DataBaseBundle\Entity\colour $colour_id = null)
    {
        $this->colour_id = $colour_id;
    
        return $this;
    }

    /**
     * Get colour_id
     *
     * @return \SB\DataBaseBundle\Entity\colour 
     */
    public function getColourId()
    {
        return $this->colour_id;
    }
     
}
 