<?php

namespace SB\DataBaseBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use SB\DataBaseBundle\Entity\Colour;
use SB\DataBaseBundle\Form\ColourType;

/**
 * Colour controller.
 *
 * @Route("/colour")
 */
class ColourController extends Controller
{

    /**
     * Lists all Colour entities.
     *
     * @Route("/", name="colour")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('SBDataBaseBundle:Colour')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new Colour entity.
     *
     * @Route("/", name="colour_create")
     * @Method("POST")
     * @Template("SBDataBaseBundle:Colour:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Colour();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('colour_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
    * Creates a form to create a Colour entity.
    *
    * @param Colour $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Colour $entity)
    {
        $form = $this->createForm(new ColourType(), $entity, array(
            'action' => $this->generateUrl('colour_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Colour entity.
     *
     * @Route("/new", name="colour_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Colour();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Colour entity.
     *
     * @Route("/{id}", name="colour_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SBDataBaseBundle:Colour')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Colour entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Colour entity.
     *
     * @Route("/{id}/edit", name="colour_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SBDataBaseBundle:Colour')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Colour entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Colour entity.
    *
    * @param Colour $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Colour $entity)
    {
        $form = $this->createForm(new ColourType(), $entity, array(
            'action' => $this->generateUrl('colour_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Colour entity.
     *
     * @Route("/{id}", name="colour_update")
     * @Method("PUT")
     * @Template("SBDataBaseBundle:Colour:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SBDataBaseBundle:Colour')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Colour entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('colour_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Colour entity.
     *
     * @Route("/{id}", name="colour_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('SBDataBaseBundle:Colour')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Colour entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('colour'));
    }

    /**
     * Creates a form to delete a Colour entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('colour_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
