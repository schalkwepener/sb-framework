<?php

namespace SB\GoogleMapGeneratorBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class DefaultController extends Controller
{
     /**
     * Lists all Uploads entities.
     *
     * @Route("/mapy", name="mapy")
     * @Method("GET")
     * @Template()
     */
    public function indexAction($name='Google Mappy Thing')
    {
        $data['lat']= '';
        $data['long']=  '';
        $data['mapy']=  '';
        return array('name' => $name, 'data' => $data);
    }
    
     /**
     * Lists all Uploads entities.
     *
     * @Route("/mapy/get", name="mapy_get")
     * @Method("POST")
     * @Template("SBGoogleMapGeneratorBundle:Default:index.html.twig")
     */
    public function mapyGetAction($name='',$lat='',$long='',$mapy='')
    {
        $request = $this->getRequest();
        
        if($name == ''){
            $name = "Woof";
        }       
        if($lat == ''){
            $lat = $request->request->get('lat');
        }       
        if($long == ''){
            $long = $request->request->get('long');
        }
        if($mapy == ''){
             $mapy = $request->request->get('mapy');
        }
       
        $data['lat']= $lat;
        $data['long']=  $long;
        $data['mapy']=  $mapy;
        
        $image_url = "http://maps.googleapis.com/maps/api/staticmap?center={$lat},{$long}&zoom=15&size=600x300&maptype=satellite&sensor=false&format=jpg&markers=color:blue%7Clabel:Sss%7C{$lat},{$long}";
       
        //$image_name = "././maps/maps_image.jpg";
        $image_name = "maps_image.jpg";
        
        $data['generated_map'] =  $image_name;
        //$location = "/var/www/html/saddam.starbright.co.za/wbc_system/web/email_attachments/" . $unique_number;
        //$full_path = $location . "/" . $filename;
        $location = "maps/" . $image_name;
      //  var_dump($image_url);
        file_put_contents($location, file_get_contents($image_url)); 
       // die();
        $data['location'] =  $location; 
        
        return array('name' => $name, 'data' => $data, );
    }
    
    
}
