<?php
// src/Acme/DemoBundle/Form/Type/GenderType.php

namespace SB\GoogleMapGeneratorBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class GoogleMapType extends AbstractType
{
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
           /* 'choices' => array(
                'm' => 'Male',
                'f' => 'Female',
            )*/
            'text' => 'woof',
            'label' => 'Registration Number 1',
            'widget' => 'googlemap_type',
            'data_class' => null
        ));
    }

    public function getParent()
    {
        return 'text';
    }

    public function getName()
    {
        return 'googlemap_type';
    }
}
?>