<?php

namespace SB\GoogleMapGeneratorBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use \Symfony\Component\DependencyInjection\ContainerInterface;

class SBGoogleMapGeneratorBundle extends Bundle
{
    private static $containerInstance = null; 

    public function setContainer(ContainerInterface $container = null) 
    { 
        parent::setContainer($container); 
        self::$containerInstance = $container; 
    }

    public static function getContainer() 
    { 
        return self::$containerInstance; 
    }
    
    public function mapyGetAction($name='',$lat='',$long='',$mapy='')
    {
        $data['lat'] = $lat;
        $data['long']=  $long;
        $data['mapy']=  $mapy;
        
        $image_url = "http://maps.googleapis.com/maps/api/staticmap?center={$lat},{$long}&zoom=15&size=600x300&maptype=satellite&sensor=false&format=jpg&markers=color:blue%7Clabel:Sss%7C{$lat},{$long}";
       
        //$image_name = "././maps/maps_image.jpg";
        $image_name = $name.".jpg";
        
        $data['generated_map'] =  $image_name;
        //$location = "/var/www/html/saddam.starbright.co.za/wbc_system/web/email_attachments/" . $unique_number;
        //$full_path = $location . "/" . $filename;
        $location = "maps/" . $image_name;
        
        file_put_contents($location, file_get_contents($image_url)); 
        
        $data['location'] =  $location; 
        $data['name'] =  $name; 
        
        return array('name' => $name, 'data' => $data, );
    }
}
