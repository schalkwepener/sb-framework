<?php

namespace SB\FrontEndBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class DefaultController extends Controller
{
    /**
     * @Route("/sbf")
     * @Template()
     */
    public function indexAction($name='')
    {
        return array('name' => $name);
    }
    /**
     * @Route("/")
     * @Template("SBFrontEndBundle:Default:visitor.html.twig")
     */
    public function visitorAction($name='')
    {
        return array('name' => $name);
    }
}
