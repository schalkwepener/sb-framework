<?php

namespace SB\FrontEndBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAware;

class LeftMenuBuilder extends ContainerAware
{
    public function leftMenu(FactoryInterface $factory, array $options)
    {

		//	List of useable Icons: http://fontawesome.io/icons/
		//
		//	Only Level 0 menu items can have Icons
		//	If no Icon is set the default Icon 'fa-circle-o' will be displayed
		//	Level 1 menu items do not have icon, if one is set it will be ignored

		// OPTIONAL EXTRAS (disabled by default)
		//
		// Badge:
		//		'extras' => array('badgeClass'=>'warning','badgeText'=>'i')
		//
		//		badgeClass options = 'default'|'primary'|'info'|'success'|'warning'|'danger'
		//		default badgeText = 'i'
		//
		// Label:
		//		'extras' => array('labelClass'=>'warning','labelText'=>'new')
		//
		//		labelClass options = 'default'|'primary'|'info'|'success'|'warning'|'danger'
		//		default labelText = 'new'
		//

        $is_admin = $this->container->get('security.context')->isGranted('ROLE_SUPER_ADMIN');

        $menu = $factory->createItem('root');
        $menu->addChild('Home',array('route' => 'homepage','extras' => array('icon' => 'fa-dashboard')));
       // $menu->addChild('Uploads',array('route' => 'uploads','extras' => array('icon' => 'fa-upload','badgeClass'=>'info','badgeText'=>'ALT+U')));

        if($is_admin){
            
            $menu->addChild('Buildboard Stuff', array('attributes' => array('class' => 'static')));
            $menu->addChild('Province',array('uri' => '#','extras' => array('icon' => 'fa-users')));
            $menu['Province']->addChild('Province', array('route' => 'province'));
            
            $menu->addChild('Illumination',array('uri' => '#','extras' => array('icon' => 'fa-users')));
            $menu['Illumination']->addChild('Illumination', array('route' => 'illumination'));
            
            $menu->addChild('Type',array('uri' => '#','extras' => array('icon' => 'fa-users')));
            $menu['Type']->addChild('Type', array('route' => 'type'));
            
            $menu->addChild('Size',array('uri' => '#','extras' => array('icon' => 'fa-users')));
            $menu['Size']->addChild('Size', array('route' => 'size'));
           
            $menu->addChild('Category',array('uri' => '#','extras' => array('icon' => 'fa-users')));
            $menu['Category']->addChild('Category', array('route' => 'category'));
            
            $menu->addChild('Rate Type',array('uri' => '#','extras' => array('icon' => 'fa-users')));
            $menu['Rate Type']->addChild('Rate Type', array('route' => 'ratetype'));
           
            $menu->addChild('BillBoards',array('route' => 'product'));
            // Preview of a static 'Header' inside the menu structure
            $menu->addChild('Settings', array('attributes' => array('class' => 'static')));
            $menu->addChild('Users',array('uri' => '#','extras' => array('icon' => 'fa-users')));
            $menu['Users']->addChild('Users', array('route' => 'user'));
            $menu['Users']->addChild('Roles', array('route' => 'role','extras' => array('labelClass'=>'danger')));
        }

        if ( $this->container->get('security.context')->isGranted('ROLE_USER')) {
           $menu->addChild('Logout', array('route' => 'logout','extras' => array('icon' => 'fa-power-off')));
           $menu->addChild('Documentation', array('uri' => '/sentir-ui-kits/master/sentir-1.0.1/index.html','extras' => array('icon' => 'fa-book')));
        }else{
           $menu->addChild('Login', array('route' => 'login','extras' => array('icon' => 'fa-power-off')));
        }

        return $menu;
    }
}
