<?php

namespace SB\FrontEndBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAware;

class TopUserMenuBuilder extends ContainerAware
{
    public function TopUserMenu(FactoryInterface $factory, array $options)
    {

		//	This menu can only have a single Tier

		if ( $this->container->get('security.context')->isGranted('ROLE_USER'))
		{

			$menu = $factory->createItem('root');

			$menu->addChild('Dashboard',array('route' => 'homepage'));
			$menu->addChild('Profile',array('uri' => '#'));

			// Preview of a static 'Divider' inside the menu structure
			$menu->addChild('Logoff Divider', array('attributes' => array('class' => 'divider')));

			$menu->addChild('Logout', array('route' => 'logout','extras' => array('icon' => 'fa-power-off')));
		}

        return $menu;
    }
}
