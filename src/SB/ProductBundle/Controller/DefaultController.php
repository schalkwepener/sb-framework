<?php

namespace SB\ProductBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class DefaultController extends Controller
{
    /**
     * @Route("/product")
     * @Template()
     */
    public function indexAction($name='proiduct')
    {
        return array('name' => $name);
    }
}
