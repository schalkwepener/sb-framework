<?php

namespace SB\ProductBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Product
 *
 * @ORM\Table(name="product")
 * @ORM\Entity
 * 
 * @Gedmo\Loggable()
 * @Gedmo\SoftDeleteable(fieldName="deletedAt")
 */

class Product
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="number", type="string", length=255)
     */
    private $number;

    /**
     * @var string
     *
     * @ORM\Column(name="site_number", type="string", length=255)
     */
    private $siteNumber;

    /**
     * @var integer
     *
     * @ORM\Column(name="province_id", type="integer")
     */
    private $provinceId;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=255)
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="suburb", type="string", length=255)
     */
    private $suburb;

    /**
     * @var string
     *
     * @ORM\Column(name="street", type="string", length=255)
     */
    private $street;

    /**
     * @var string
     *
     * @ORM\Column(name="cnr_street", type="string", length=255)
     */
    private $cnrStreet;

    /**
     * @var string
     *
     * @ORM\Column(name="gps_long", type="string", length=255)
     */
    private $gpsLong;

    /**
     * @var string
     *
     * @ORM\Column(name="gps_lat", type="string", length=255)
     */
    private $gpsLat;

    /**
     * @var integer
     *
     * @ORM\Column(name="illumination_id", type="integer")
     */
    private $illuminationId;

    /**
     * @var integer
     *
     * @ORM\Column(name="type_id", type="integer")
     */
    private $typeId;

    /**
     * @var integer
     *
     * @ORM\Column(name="size_id", type="integer")
     */
    private $sizeId;

    /**
     * @var integer
     *
     * @ORM\Column(name="cateogry_id", type="integer")
     * @Gedmo\Versioned
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="product")
     * @ORM\JoinColumn(name="cateogry_id", referencedColumnName="id")
     */
    private $categoryId;

    /**
     * @var string
     *
     * @ORM\Column(name="rate_type", type="string", length=255)
     */
    private $rateType;

    /**
     * @var integer
     *
     * @ORM\Column(name="template_id", type="integer")
     */
    private $templateId;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Product
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set number
     *
     * @param string $number
     * @return Product
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return string 
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set siteNumber
     *
     * @param string $siteNumber
     * @return Product
     */
    public function setSiteNumber($siteNumber)
    {
        $this->siteNumber = $siteNumber;

        return $this;
    }

    /**
     * Get siteNumber
     *
     * @return string 
     */
    public function getSiteNumber()
    {
        return $this->siteNumber;
    }

    /**
     * Set provinceId
     *
     * @param integer $provinceId
     * @return Product
     */
    public function setProvinceId($provinceId)
    {
        $this->provinceId = $provinceId;

        return $this;
    }

    /**
     * Get provinceId
     *
     * @return integer 
     */
    public function getProvinceId()
    {
        return $this->provinceId;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return Product
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string 
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set suburb
     *
     * @param string $suburb
     * @return Product
     */
    public function setSuburb($suburb)
    {
        $this->suburb = $suburb;

        return $this;
    }

    /**
     * Get suburb
     *
     * @return string 
     */
    public function getSuburb()
    {
        return $this->suburb;
    }

    /**
     * Set street
     *
     * @param string $street
     * @return Product
     */
    public function setStreet($street)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * Get street
     *
     * @return string 
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Set cnrStreet
     *
     * @param string $cnrStreet
     * @return Product
     */
    public function setCnrStreet($cnrStreet)
    {
        $this->cnrStreet = $cnrStreet;

        return $this;
    }

    /**
     * Get cnrStreet
     *
     * @return string 
     */
    public function getCnrStreet()
    {
        return $this->cnrStreet;
    }

    /**
     * Set gpsLong
     *
     * @param string $gpsLong
     * @return Product
     */
    public function setGpsLong($gpsLong)
    {
        $this->gpsLong = $gpsLong;

        return $this;
    }

    /**
     * Get gpsLong
     *
     * @return string 
     */
    public function getGpsLong()
    {
        return $this->gpsLong;
    }

    /**
     * Set gpsLat
     *
     * @param string $gpsLat
     * @return Product
     */
    public function setGpsLat($gpsLat)
    {
        $this->gpsLat = $gpsLat;

        return $this;
    }

    /**
     * Get gpsLat
     *
     * @return string 
     */
    public function getGpsLat()
    {
        return $this->gpsLat;
    }

    /**
     * Set illuminationId
     *
     * @param integer $illuminationId
     * @return Product
     */
    public function setIlluminationId($illuminationId)
    {
        $this->illuminationId = $illuminationId;

        return $this;
    }

    /**
     * Get illuminationId
     *
     * @return integer 
     */
    public function getIlluminationId()
    {
        return $this->illuminationId;
    }

    /**
     * Set typeId
     *
     * @param integer $typeId
     * @return Product
     */
    public function setTypeId($typeId)
    {
        $this->typeId = $typeId;

        return $this;
    }

    /**
     * Get typeId
     *
     * @return integer 
     */
    public function getTypeId()
    {
        return $this->typeId;
    }

    /**
     * Set sizeId
     *
     * @param integer $sizeId
     * @return Product
     */
    public function setSizeId($sizeId)
    {
        $this->sizeId = $sizeId;

        return $this;
    }

    /**
     * Get sizeId
     *
     * @return integer 
     */
    public function getSizeId()
    {
        return $this->sizeId;
    }

    /**
     * Set categoryId
     *
     * @param integer $categoryId
     * @return Product
     */
    public function setCategoryId($categoryId)
    {
        $this->categoryId = $categoryId;

        return $this;
    }

    /**
     * Get categoryId
     *
     * @return integer 
     */
    public function getCategoryId()
    {
        return $this->categoryId;
    }

    /**
     * Set rateType
     *
     * @param string $rateType
     * @return Product
     */
    public function setRateType($rateType)
    {
        $this->rateType = $rateType;

        return $this;
    }

    /**
     * Get rateType
     *
     * @return string 
     */
    public function getRateType()
    {
        return $this->rateType;
    }

    /**
     * Set templateId
     *
     * @param integer $templateId
     * @return Product
     */
    public function setTemplateId($templateId)
    {
        $this->templateId = $templateId;

        return $this;
    }

    /**
     * Get templateId
     *
     * @return integer 
     */
    public function getTemplateId()
    {
        return $this->templateId;
    }
}
