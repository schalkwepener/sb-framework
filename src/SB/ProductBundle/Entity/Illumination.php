<?php

namespace SB\ProductBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Illumination
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Illumination
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="illumination_type", type="string", length=255)
     */
    private $illuminationType;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set illuminationType
     *
     * @param string $illuminationType
     * @return Illumination
     */
    public function setIlluminationType($illuminationType)
    {
        $this->illuminationType = $illuminationType;

        return $this;
    }

    /**
     * Get illuminationType
     *
     * @return string 
     */
    public function getIlluminationType()
    {
        return $this->illuminationType;
    }
}
