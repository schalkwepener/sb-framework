## php composer.phar update

php app/console doctrine:schema:update --force
php app/console cache:clear --env=dev

rm -d web/bundles/

php app/console assets:install --symlink
# php app/console assetic:dump